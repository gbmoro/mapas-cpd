package cpd.ufrgs_maps.ui.mapscreen.tasks;

import android.os.AsyncTask;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

import cpd.ufrgs_maps.model.BuildingDAO;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.ui.mapscreen.MapsActivity;

/**
 * Created by gabrieldoandroid on 05/06/17.
 */

public class DrawMarker extends AsyncTask<Void, Void, Void> {

    MapsActivity mapsActivity;
    BuildingDAO buildingDAO;
    BuildingPOJO buildingPOJO;

    public DrawMarker(MapsActivity mapsActivity, BuildingPOJO buildingPOJOP) {
        this.mapsActivity = mapsActivity;
        this.buildingDAO = new BuildingDAO(mapsActivity);
        this.buildingPOJO = buildingPOJOP;
    }

    @Override
    protected Void doInBackground(Void... params) {

        List<BuildingPOJO> buildingPOJOList = buildingDAO.select(buildingPOJO.latitude, buildingPOJO.longitude);

        LatLng tmp = null;


        tmp = new LatLng(buildingPOJO.latitude, buildingPOJO.longitude);

        this.mapsActivity.addMarker(tmp, buildingPOJO,(buildingPOJOList!=null && buildingPOJOList.size() > 1));

        return null;
    }
}
