package cpd.ufrgs_maps.ui.mapscreen.clustering;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;

/**
 * This class is required to insert the markers into
 * the cluster approach.
 * Created by gabrielbmoro on 08/05/17.
 */
public class ItemOfMap implements ClusterItem {

    LatLng mPosition;
    BuildingPOJO buildingTarget;
    boolean isMoreRegions;

    /**
     * The constructor method is used to configure
     * for each marker its own Building object.
     * @param buildingTargetT
     * @param latLng
     */
    public ItemOfMap(BuildingPOJO buildingTargetT, LatLng latLng, boolean isMoreRegionsHere) {
        this.mPosition = latLng;
        this.buildingTarget = buildingTargetT;
        this.isMoreRegions = isMoreRegionsHere;
    }

    /**
     * The getter method returns the position.
     * @return
     */
    @Override
    public LatLng getPosition() {
        return mPosition;
    }

    /**
     * The getter method returns the building of
     * the position.
     * @return
     */
    public BuildingPOJO getBuildingTarget() {
        return buildingTarget;
    }

    public boolean getIsMoreRegions() {
        return this.isMoreRegions;
    }

}
