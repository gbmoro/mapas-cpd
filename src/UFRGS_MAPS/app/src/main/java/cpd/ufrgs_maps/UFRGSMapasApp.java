package cpd.ufrgs_maps;

import android.app.Application;
import cpd.ufrgs_maps.dagger.component.DaggerNetComponent;
import cpd.ufrgs_maps.dagger.component.NetComponent;
import cpd.ufrgs_maps.dagger.module.AppModule;
import cpd.ufrgs_maps.dagger.module.NetModule;


/**
 * This is the App's main class, the first to be executed
 * Created by gabrielbmoro on 06/05/17.
 */
public class UFRGSMapasApp extends Application {

    private NetComponent mNetComponent;
    static final String BASE_URL = "https://desenvolvimento.dsi/ws/siteufrgs/";

    /**
     * This is a method responsible for inital configuration
     * of the frameworks or libraries used for the app.
     */
    @Override
    public void onCreate() {
        super.onCreate();

        mNetComponent = DaggerNetComponent.builder()
                .appModule(new AppModule(this))
                .netModule(new NetModule(BASE_URL, getApplicationContext()))
                .build();
    }

    /**
     * This getter method is called class to be injected.
     * @return A object NetComponent
     */
    public NetComponent getNetComponent() {
        return mNetComponent;
    }

}
