package cpd.ufrgs_maps.ui.listbuildingsscreen.adapter;

import android.content.Context;
import android.graphics.BitmapFactory;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

import java.util.List;
import cpd.ufrgs_maps.R;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.model.CampusType;

/**
 * Created by gabrielbmoro on 04/06/17.
 */

public class BuildingAdapter extends BaseAdapter{

    List<BuildingPOJO> buildingVOList;
    Context mContext;

    public BuildingAdapter(Context context, List<BuildingPOJO> buildingVOList) {
        this.mContext = context;
        this.buildingVOList = buildingVOList;
    }

    @Override
    public int getCount() {
        return buildingVOList.size();
    }

    @Override
    public Object getItem(int position) {
        return buildingVOList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return buildingVOList.get(position).hashCode();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        BuildingPOJO buildingPOJO = buildingVOList.get(position);

        LayoutInflater inflater = LayoutInflater.from(mContext);
        View view = convertView;

        if(view == null) {
            view = inflater.inflate(R.layout.list_building_item, parent, false);
        }

        TextView txtBuildingName = (TextView) view.findViewById(R.id.buildingname);
        TextView txtBuildingCampus = (TextView) view.findViewById(R.id.txtCampusType);
        TextView txtBuildingNumber = (TextView) view.findViewById(R.id.buildPredioNumber);
        TextView txtBuildingAddress = (TextView) view.findViewById(R.id.buildingend);
        TextView txtBuildingOTherInfo = (TextView) view.findViewById(R.id.buildingotherinfo);

        ImageView imgView = (ImageView) view.findViewById(R.id.imgbuilding);

        String addressTarget = buildingPOJO.logradouro;
        String otherInfo = buildingPOJO.cidade;

        if(!buildingPOJO.nrLogradouro.isEmpty()) {
            addressTarget = addressTarget + ", " + buildingPOJO.nrLogradouro;
        }

        if(!buildingPOJO.cEP.isEmpty()) {
                otherInfo = otherInfo + ", CEP: " + buildingPOJO.cEP;
        }

        txtBuildingName.setText(buildingPOJO.nomePredio);
        txtBuildingCampus.setText(buildingPOJO.denominacaoCampus);
        txtBuildingAddress.setText(addressTarget);
        txtBuildingOTherInfo.setText(otherInfo);
        txtBuildingNumber.setText("Prédio: " + buildingPOJO.codPredioUFRGS);

        switch (buildingPOJO.campus) {
            case CampusType.CENTRO:
                imgView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.centro));
                break;
            case CampusType.OLIMPICO:
                imgView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.olimpico));
                break;
            case CampusType.SAUDE:
                imgView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.saude));
                break;
            case CampusType.VALE:
                imgView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),
                        R.drawable.vale));
                break;
            default:
                    imgView.setImageBitmap(BitmapFactory.decodeResource(mContext.getResources(),
                            R.drawable.centro));
                break;
        }

        return view;
    }

}
