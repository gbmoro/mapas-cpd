package cpd.ufrgs_maps.model;

import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.text.NumberFormat;

/**
 * Created by gabrielbmoro on 04/06/17.
 */

public class Conversor {

    public static int convertToInteger(String value) {
        return Integer.parseInt(value);
    }

    public static double convertToDouble(String value) {
        double result = 0;
        if(value!=null) {
            value = value.replace('\"', ' ');
            value = value.trim();
            value = value.replace(',', '.');
            result = Double.parseDouble(value);
        }
        return result;
    }


    public static String format(Number n) {
        NumberFormat format = DecimalFormat.getInstance();
        format.setRoundingMode(RoundingMode.FLOOR);
        format.setMinimumFractionDigits(0);
        format.setMaximumFractionDigits(2);
        return format.format(n);
    }

}
