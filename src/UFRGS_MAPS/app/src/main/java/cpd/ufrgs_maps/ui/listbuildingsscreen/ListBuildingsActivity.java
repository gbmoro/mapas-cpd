package cpd.ufrgs_maps.ui.listbuildingsscreen;

import android.content.Intent;
import android.support.v4.app.NavUtils;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import cpd.ufrgs_maps.R;
import cpd.ufrgs_maps.model.BuildingDAO;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.ui.listbuildingsscreen.adapter.BuildingAdapter;

public class ListBuildingsActivity extends AppCompatActivity {

    @BindView(R.id.listBuildings)
    ListView listView;
    List<BuildingPOJO> buildingVOList;
    BuildingDAO buildingDAO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_buildings);

        ButterKnife.bind(this);


        if(getIntent().getSerializableExtra("buildingTarget")==null) {

            buildingDAO = new BuildingDAO(this);

            double latitude = getIntent().getExtras().getDouble(BuildingDAO.KEY_LATITUDE);
            double longitude = getIntent().getExtras().getDouble(BuildingDAO.KEY_LONGITUDE);

            if (latitude != 0 && longitude != 0) {
                buildingVOList = buildingDAO.select(latitude, longitude);
                Log.i(getClass().getName(), latitude + ":" + longitude);
                Log.i(getClass().getName(), "Size of list: " + String.valueOf(buildingVOList.size()));

                listView.setAdapter(new BuildingAdapter(this, buildingVOList));

            } else {
                finish();
            }
        } else {

            BuildingPOJO b = (BuildingPOJO) getIntent().getSerializableExtra("buildingTarget");
            buildingVOList = new ArrayList<BuildingPOJO>();
            buildingVOList.add(b);
            listView.setAdapter(new BuildingAdapter(this, buildingVOList));

            Toast.makeText(this, "Lat: " + b.latitude + ", Long: "+ b.longitude, Toast.LENGTH_SHORT)
            .show();
        }

        getSupportActionBar().setHomeButtonEnabled(true);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                NavUtils.navigateUpFromSameTask(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
    }

}
