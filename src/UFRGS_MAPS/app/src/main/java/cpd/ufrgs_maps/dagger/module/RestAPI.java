package cpd.ufrgs_maps.dagger.module;

import java.util.List;

import cpd.ufrgs_maps.model.vo.BuildingVO;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * This class represents the called to
 * server and the return of it.
 * Created by gabrielbmoro on 05/05/17.
 */
public interface RestAPI {
    /**
     * This method gets all buildings
     * from server.
     * @return the List with all buildings in VO format
     */
    @GET("getPredios")
    Call<List<BuildingVO>> getAllBuildings();

}
