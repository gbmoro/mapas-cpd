package cpd.ufrgs_maps.model;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.util.Log;
import com.google.android.gms.maps.model.LatLng;
import java.util.List;
import java.util.Locale;

import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.model.vo.BuildingVO;

/**
 * Created by gabrielbmoro on 04/06/17.
 */

public class Localizator {

    static final int RESULTS_OF_GEOCODER = 1;
    Context mContext;

    /**
     * The constructor method receives a context
     * @param context
     */
    public Localizator(Context context) {
        this.mContext = context;
    }

    /**
     * The LatLng is the object used for GMaps API
     * as localization.
     * @return of LatLng
     */
    public LatLng getMyLocation() {
        LocationManager locationManager = (LocationManager) mContext
                .getSystemService(mContext.LOCATION_SERVICE);
        Location location = null;
        if (ActivityCompat.checkSelfPermission(mContext, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(mContext
                , Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return null;
        } else {
            location = locationManager.getLastKnownLocation(locationManager.getBestProvider(new Criteria(), true));
            if (location != null) {
                return new LatLng(location.getLatitude(), location.getLongitude());
            } else {
                return null;
            }
        }
    }

    /**
     * The value returned is an object of LatLng type.
     * @param addressFormated of String
     * @return of LatLng
     */
    public LatLng getLocalization(String addressFormated) {

        LatLng posicao = null;

        try {
            Geocoder geocoder = new Geocoder(mContext, Locale.getDefault());

            List<Address> resultados =
                    geocoder.getFromLocationName(addressFormated, RESULTS_OF_GEOCODER);
            if (!resultados.isEmpty() && resultados!=null) {
                posicao = new LatLng(resultados.get(0).getLatitude(), resultados.get(0).getLongitude());
            }
        } catch (Exception e) {
            Log.e(getClass().getName(), e.getMessage());
            return null;
        }
        return posicao;
    }

    public BuildingPOJO convertVOtoPOJO(BuildingVO buildingVO) {
        BuildingPOJO buildingPOJO = null;

        try {
            buildingPOJO = new BuildingPOJO();

            buildingPOJO.campus = Integer.parseInt(buildingVO.campus);
            buildingPOJO.denominacaoCampus = buildingVO.denominacaoCampus;

            if(buildingVO.cEP==null) {
                buildingPOJO.cEP = "";
            } else {
                buildingPOJO.cEP = buildingVO.cEP;
            }

            buildingPOJO.codPredio = buildingVO.codPredio;
            buildingPOJO.nomePredio = buildingVO.nomePredio;

            buildingPOJO.latitude = Double.parseDouble(buildingVO.latitude);
            buildingPOJO.longitude = Double.parseDouble(buildingVO.longitude);

            buildingPOJO.codPredioUFRGS = buildingVO.codPredioUFRGS;
            buildingPOJO.logradouro = buildingVO.logradouro.replaceAll(",", "");
            buildingPOJO.nrLogradouro = buildingVO.nrLogradouro;
            if(buildingVO.cidade==null) {
                buildingPOJO.cidade = "Porto Alegre";
            } else {
                buildingPOJO.cidade = buildingVO.cidade;
            }

            buildingPOJO.telefone = buildingVO.telefone;

        } catch(NullPointerException erro) {
            Log.e(getClass().getName() , erro.getMessage());
        }

        return buildingPOJO;
    }

}
