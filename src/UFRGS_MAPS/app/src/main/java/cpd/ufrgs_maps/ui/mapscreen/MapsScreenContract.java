package cpd.ufrgs_maps.ui.mapscreen;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.LatLng;

import cpd.ufrgs_maps.model.pojo.BuildingPOJO;

/**
 * Created by gabri on 13/06/2017.
 */

public interface MapsScreenContract {

    interface View {
        void addMarker(LatLng latLng, BuildingPOJO building, boolean isMoreBuildings);
        void moveWithCamera();
        void configureVoiceSearch();
        void loadHistory(String query);
    }

    interface Presenter {
        void findBySearchView();
        void markerEvent(GoogleMap mMap);
    }

}
