package cpd.ufrgs_maps.ui.mapscreen;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.MatrixCursor;
import android.os.Build;
import android.support.v4.app.FragmentActivity;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.SearchView;
import android.view.Menu;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterManager;
import java.util.ArrayList;
import java.util.List;
import javax.inject.Inject;
import butterknife.BindView;
import butterknife.ButterKnife;
import cpd.ufrgs_maps.R;
import cpd.ufrgs_maps.UFRGSMapasApp;
import cpd.ufrgs_maps.model.BuildingDAO;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.model.Localizator;
import cpd.ufrgs_maps.ui.mapscreen.adapter.FilterAdapter;
import cpd.ufrgs_maps.ui.mapscreen.clustering.ItemOfMap;
import cpd.ufrgs_maps.ui.mapscreen.clustering.OurClusterRenderer;
import cpd.ufrgs_maps.ui.mapscreen.tasks.CalledToServerTask;
import cpd.ufrgs_maps.ui.mapscreen.tasks.DrawMarker;
import retrofit2.Retrofit;

public class MapsActivity extends FragmentActivity implements OnMapReadyCallback, MapsScreenContract.View {

    private GoogleMap mMap;
    BuildingDAO buildingDAO;
    @BindView(R.id.searchbar)
    SearchView searchView;
    @Inject
    Retrofit retrofit;
    ClusterManager<ItemOfMap> mClusterManager;
    Localizator localizator;
    static int ZOOM_CONSTANTE_VALUE = 18;
    MapsScreenPresenter mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_maps);
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.

        buildingDAO = new BuildingDAO(this);
        localizator = new Localizator(this);

        ((UFRGSMapasApp) this.getApplication()).getNetComponent().inject(this);

        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        ButterKnife.bind(this);
        configureVoiceSearch();
        mPresenter = new MapsScreenPresenter(this, searchView);
        mPresenter.findBySearchView();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera. In this case,
     * we just add a marker near Sydney, Australia.
     * If Google Play services is not installed on the device, the user will be prompted to install
     * it inside the SupportMapFragment. This method will only be triggered once the user has
     * installed Google Play services and returned to the app.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mClusterManager = new ClusterManager<ItemOfMap>(this, mMap);
        mClusterManager.setRenderer(new OurClusterRenderer(this, googleMap, mClusterManager));
        mMap.setOnMarkerClickListener(mClusterManager);
        mMap.setOnInfoWindowClickListener(mClusterManager);
        mMap.setInfoWindowAdapter(mClusterManager.getMarkerManager());
        mMap.setOnInfoWindowClickListener(mClusterManager);

        moveWithCamera();


        List<BuildingPOJO> buildingPOJOList = buildingDAO.select();
        if(buildingPOJOList==null || buildingPOJOList.isEmpty()) {
            new CalledToServerTask(this, retrofit).execute();
        }else {
            LatLng target = null, tmp = null;

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                    == PackageManager.PERMISSION_GRANTED) {
                mMap.setMyLocationEnabled(true);
            }

            mMap.getUiSettings().setZoomGesturesEnabled(true);
            mMap.getUiSettings().setZoomControlsEnabled(true);
            mMap.getUiSettings().setCompassEnabled(true);
            mMap.getUiSettings().setScrollGesturesEnabled(true);
            mMap.getUiSettings().setTiltGesturesEnabled(true);

            mMap.setOnCameraIdleListener(mClusterManager);

            for(BuildingPOJO buildingPOJOTmp : buildingPOJOList) {
                new DrawMarker(this, buildingPOJOTmp).execute();
            }

            this.mPresenter.markerEvent(mMap);
        }
    }

    @Override
    public void moveWithCamera() {
        // Add a marker in Sydney and move the camera
        LatLng reitoria = new LatLng(-30.03389450, -51.21890610);
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(reitoria, ZOOM_CONSTANTE_VALUE));
    }

    @Override
    public void configureVoiceSearch() {
        this.searchView.setQueryHint("Buscar no UFRGS MAPAS");
    }

    @Override
    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public void loadHistory(String query) {
        List<BuildingPOJO> buildingPOJOList = buildingDAO.select();

        List<BuildingPOJO> buildigsTarget = new ArrayList<BuildingPOJO>();

        for(BuildingPOJO buildingPOJOTmp : buildingPOJOList){
            if(buildingPOJOTmp.nomePredio!=null) {
                if (buildingPOJOTmp.nomePredio.toLowerCase().contains(query.toLowerCase()) ||
                        buildingPOJOTmp.denominacaoCampus.toLowerCase().contains(query.toLowerCase())) {
                    buildigsTarget.add(buildingPOJOTmp);
                }
            }
        }

        if(Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {

            String[] columns = new String[]{"_id", "description"};
            Object[] tmp = new Object[]{0, "default"};

            MatrixCursor cursor = new MatrixCursor(columns);

            for (int x = 0; x < buildigsTarget.size(); x++) {
                tmp[0] = x;
                tmp[1] = buildingPOJOList.get(x).nomePredio;
                cursor.addRow(tmp);
            }

            searchView.setSuggestionsAdapter(new FilterAdapter(this, cursor, buildigsTarget));
            SearchManager searchManager = (SearchManager) getSystemService(Context.SEARCH_SERVICE);
            searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        }
    }
    @Override
    protected void onNewIntent(Intent intent) {
        if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
            String query = searchView.getQuery().toString();
            loadHistory(query);
        }
    }


    /**
     * The method inserts a new marker on Map (using
     * the clustering approach
     * @param latLng of LatLng
     * @param building of BuildingPOJO
     */
    @Override
    public void addMarker(LatLng latLng, BuildingPOJO building, boolean isMoreBuildings) {

        if(latLng.longitude==0 || latLng.longitude==0) {
            return;
        }

        ItemOfMap offSetItem = new ItemOfMap(building, latLng, isMoreBuildings);
        mClusterManager.addItem(offSetItem);

    }

    public void restart() {
        Intent intent = getIntent();
        finish();
        startActivity(intent);
    }

}
