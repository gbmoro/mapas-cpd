package cpd.ufrgs_maps.dagger.component;

import javax.inject.Singleton;

import cpd.ufrgs_maps.ui.mapscreen.MapsActivity;
import cpd.ufrgs_maps.dagger.module.AppModule;
import cpd.ufrgs_maps.dagger.module.NetModule;
import dagger.Component;

/**
 * This component is created from modules: AppModule, and NetModule.
 * Created by gabrielbmoro on 06/05/17.
 */
@Singleton
@Component(modules = {AppModule.class, NetModule.class})
public interface NetComponent {

    void inject(MapsActivity activity);

}
