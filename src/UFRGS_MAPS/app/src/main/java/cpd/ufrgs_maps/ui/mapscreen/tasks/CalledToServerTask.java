package cpd.ufrgs_maps.ui.mapscreen.tasks;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import java.io.IOException;
import java.security.cert.CertPathValidatorException;
import java.util.ArrayList;
import java.util.List;
import cpd.ufrgs_maps.dagger.module.RestAPI;
import cpd.ufrgs_maps.model.BuildingDAO;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.model.vo.BuildingVO;
import cpd.ufrgs_maps.model.Localizator;
import cpd.ufrgs_maps.ui.mapscreen.MapsActivity;
import retrofit2.Call;
import retrofit2.Response;
import retrofit2.Retrofit;

/**
 * The thread responsible for called to server.
 * In this case, the Android thread is called AsyncTask,
 * which is executed in background, as a service.
 * Created by gabrieldoandroid on 5/18/17.
 */
public class CalledToServerTask extends AsyncTask<Void, Void, Void> {

    MapsActivity mapsActivity;
    Context mContext;
    Retrofit retrofit;
    List<BuildingPOJO> buildingPOJOList;
    BuildingDAO buildingDAO;
    ProgressDialog progressDialog;
    Localizator localizator;
    static final int AMOUNT_OF_BUILDING = 248;
    int buildingCounter;

    /**
     * The constructor method needs two objects, the context
     * (reference of the Activity) and the retrofit object to
     * create the connection to server.
     * @param mapsActivity of Activity
     * @param retrofit of Connection
     */
    public CalledToServerTask(MapsActivity mapsActivity, Retrofit retrofit) {
        this.mContext = mapsActivity.getApplicationContext();
        this.mapsActivity = mapsActivity;
        this.retrofit = retrofit;
        this.buildingDAO = new BuildingDAO(mapsActivity);
        this.progressDialog = new ProgressDialog(mapsActivity);
        this.localizator = new Localizator(mContext);
        this.buildingPOJOList = new ArrayList<>();
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        progressDialog.setTitle("Aguarde");
        progressDialog.setMessage("Estamos buscando todas as informações para você!");
        progressDialog.setIndeterminate(false);
        progressDialog.setMax(AMOUNT_OF_BUILDING);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
        progressDialog.setCancelable(false);
        progressDialog.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        progressDialog.setProgress(this.progressDialog.getProgress() + 1);
    }

    /**
     * This method has the same approach that method run
     * (Runnable classes). We may to call this method as
     * the main method of this class.
     * @param params
     * @return
     */
    @Override
    protected Void doInBackground(Void... params) {

        BuildingPOJO buildingPOJO;
        Call<List<BuildingVO>> callToServer = retrofit.create(RestAPI.class).getAllBuildings();

        try {
            Response<List<BuildingVO>> response = callToServer.execute();
            if (response != null) {

                for(BuildingVO localizationVOTmp : response.body()) {
                             buildingPOJO = this.localizator
                                    .convertVOtoPOJO(localizationVOTmp);
                            if(buildingPOJO!=null) {
                                this.buildingPOJOList.add(buildingPOJO);
                                buildingCounter++;
                                publishProgress();
                            }
                        }
                Log.i(getClass().getName(), "Response != nulo, size = " + response.body().size());
            }
        } catch (Exception e) {
            Log.e("Error", e.getMessage());
            mapsActivity.finish();
        }

        return null;
    }

    /**
     * After the execution of the doInBackground method,
     * this method is called immediately.
     * @param aVoid
     */
    @Override
    protected void onPostExecute(Void aVoid) {
        super.onPostExecute(aVoid);
        this.progressDialog.dismiss();

        for(BuildingPOJO buildingPOJO : buildingPOJOList)
            buildingDAO.insert(buildingPOJO);

        mapsActivity.restart();
    }
}
