package cpd.ufrgs_maps.model.pojo;

import java.io.Serializable;

/**
 * Created by gabrieldoandroid on 12/06/17.
 */
public class BuildingPOJO implements Serializable{

    public String codPredio;
    public String nomePredio;
    public String codPredioUFRGS;
    public int campus;
    public String denominacaoCampus;
    public double latitude;
    public double longitude;
    public String logradouro;
    public String nrLogradouro;
    public String cidade;
    public String cEP;
    public String telefone;

}
