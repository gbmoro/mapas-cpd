package cpd.ufrgs_maps.model;

/**
 * Created by gabrieldoandroid on 12/06/17.
 */

public class CampusType {

    public static final int CENTRO = 1;
    public static final int SAUDE = 2;
    public static final int OLIMPICO = 3;
    public static final int VALE = 4;
    public static final int UNIDADES_ISOLADAS = 5;

}
