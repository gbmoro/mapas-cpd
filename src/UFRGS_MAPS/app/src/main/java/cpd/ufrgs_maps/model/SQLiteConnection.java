package cpd.ufrgs_maps.model;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * This class is the Connection Manager of the database.
 * Created by gabriel-moro on 18/04/17.
 */
public class SQLiteConnection extends SQLiteOpenHelper {

    public static SQLiteConnection myInstance;
    public static int OFFICIAL_VERSION = 1;
    public static final String DATABASE_NAME = "ufrgsmaps";
    private static final String queryToCreateBuildingTable = "CREATE TABLE " + BuildingDAO.TABLE_NAME + " (" +
            BuildingDAO.KEY_CODPREDIO + " TEXT, " +
            BuildingDAO.KEY_NOMEPREDIO + " TEXT, " +
            BuildingDAO.KEY_CODPREDIOUFRGS + " TEXT, " +
            BuildingDAO.KEY_CAMPUS + " INTEGER, " +
            BuildingDAO.KEY_DENOMINACAOCAMPUS + " TEXT, " +
            BuildingDAO.KEY_LATITUDE + " REAL, " +
            BuildingDAO.KEY_LONGITUDE + " REAL, " +
            BuildingDAO.KEY_LOGRADOURO + " TEXT, " +
            BuildingDAO.KEY_NRLOGRADOURO + " TEXT, " +
            BuildingDAO.KEY_CIDADE + " TEXT, " +
            BuildingDAO.KEY_CEP + " TEXT, " +
            BuildingDAO.KEY_TELEFONE + " TEXT);";


    private static final String queryToDropBuildingTable = "DROP TABLE " + BuildingDAO.TABLE_NAME + ";";

    /**
     * The constructor method is used to setting the SQLite's initial configuration
     * @param context of Activity that will use the connection
     * @param name is used to identify the database (of String)
     * @param factory allows to create the instance of control to SQLite
     * @param version is used to control the updates in Database structure
     */
    public SQLiteConnection(Context context, String name, SQLiteDatabase.CursorFactory factory, int version) {
        super(context, name, factory, version);
    }

    /**
     * The singleton pattern is used to control that only one instance will be used
     * for app as whole.
     * @param context of Activity
     * @return SQLiteConnection will be used for BuildingDAO (for example)
     */
    public static SQLiteConnection getMyInstance(Context context) {
        if (myInstance == null) {
            myInstance = new SQLiteConnection(context, DATABASE_NAME, null, OFFICIAL_VERSION);
        }
        return myInstance;
    }

    /**
     * The method onCreate is the first method executed in this class,
     * after the constructor method.
     * @param db of SQLiteDataBase
     */
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(queryToCreateBuildingTable);
        Log.i(getClass().getName(), "Data base was created...");
    }

    /**
     * This method hasn't implementation
     * @param db
     * @param oldVersion
     * @param newVersion
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

    }

    /**
     * This method allows to clean all data on database.
     */
    public void clearAllData() {
        this.getWritableDatabase().execSQL(queryToDropBuildingTable);
        this.getWritableDatabase().execSQL(queryToCreateBuildingTable);
        Log.i(getClass().getName(), "All data were deleted...");
    }

}