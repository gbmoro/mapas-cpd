package cpd.ufrgs_maps.ui.mapscreen.adapter;

import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import java.util.ArrayList;
import java.util.List;
import cpd.ufrgs_maps.R;
import cpd.ufrgs_maps.model.pojo.BuildingPOJO;
import cpd.ufrgs_maps.ui.listbuildingsscreen.ListBuildingsActivity;

/**
 * Created by gabrieldoandroid on 14/06/17.
 */

public class FilterAdapter extends CursorAdapter {

    List<BuildingPOJO> buildingPOJOsList;
    Context mContext;


    public FilterAdapter(Context context, Cursor cursor, List<BuildingPOJO> pojoList) {
        super(context, cursor, false);
        this.mContext = context;
        this.buildingPOJOsList = pojoList;
    }

    @Override
    public View newView(Context context, Cursor cursor, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater)
                context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = inflater.inflate(R.layout.itemfilter, parent, false);
        return view;
    }

    @Override
    public void bindView(View view, Context context, Cursor cursor) {
        final Button mButton = (Button) view.findViewById(R.id.textBuildingName);
        mButton.setText(buildingPOJOsList.get(cursor.getPosition()).nomePredio);

        mButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String target = mButton.getText().toString();

                for (BuildingPOJO buildingTMp : buildingPOJOsList) {
                    if (buildingTMp.nomePredio.equalsIgnoreCase(target)) {
                        Intent intent = new Intent(mContext, ListBuildingsActivity.class);
                        intent.putExtra("buildingTarget", buildingTMp);
                        mContext.startActivity(intent);
                    }
                }

            }
        });
    }
}
