package cpd.ufrgs_maps.ui.mapscreen.clustering;

import android.content.Context;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;

import cpd.ufrgs_maps.R;
import cpd.ufrgs_maps.model.BuildingDAO;

/**
 * Created by gabrieldoandroid on 5/22/17.
 */

public class OurClusterRenderer extends DefaultClusterRenderer<ItemOfMap> {

    BuildingDAO buildingDAO;

    public OurClusterRenderer(Context context, GoogleMap map, ClusterManager<ItemOfMap> clusterManager) {
        super(context, map, clusterManager);
    }

    @Override
    protected void onClusterItemRendered(ItemOfMap clusterItem, Marker marker) {
        super.onClusterItemRendered(clusterItem, marker);
    }

    @Override
    protected void onBeforeClusterItemRendered(ItemOfMap item, MarkerOptions markerOptions) {
        String address = item.getBuildingTarget().logradouro + ", " +
                item.getBuildingTarget().nrLogradouro;
        markerOptions.title(address);
        markerOptions.snippet("Campus: " + item.getBuildingTarget().denominacaoCampus);

        markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin02));

        if(item.getIsMoreRegions()) {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin03));
        } else {
            markerOptions.icon(BitmapDescriptorFactory.fromResource(R.drawable.pin02));
        }
    }

    @Override
    protected void onBeforeClusterRendered(Cluster<ItemOfMap> cluster, MarkerOptions markerOptions) {
        super.onBeforeClusterRendered(cluster, markerOptions);
    }

    @Override
    protected boolean shouldRenderAsCluster(Cluster<ItemOfMap> cluster) {
        //If markers near are > 4 a cluster will be created.
        return cluster.getSize() > 19;
    }

    @Override
    protected int getBucket(Cluster<ItemOfMap> cluster) {
        // show exact number of items in cluster's bubble
        return cluster.getSize();
    }
}