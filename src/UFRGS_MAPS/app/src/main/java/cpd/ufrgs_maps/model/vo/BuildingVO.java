package cpd.ufrgs_maps.model.vo;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * This class represents a building,
 * and its info.
 * The BuildingVO object is the POJO used in the app
 * to represent a building.
 * Created by gabriel-moro on 10/04/17.
 */
public class BuildingVO {

        @SerializedName("CodPredio")
        @Expose
        public String codPredio;
        @SerializedName("NomePredio")
        @Expose
        public String nomePredio;
        @SerializedName("CodPredioUFRGS")
        @Expose
        public String codPredioUFRGS;
        @SerializedName("Campus")
        @Expose
        public String campus;
        @SerializedName("DenominacaoCampus")
        @Expose
        public String denominacaoCampus;
        @SerializedName("Latitude")
        @Expose
        public String latitude;
        @SerializedName("Longitude")
        @Expose
        public String longitude;
        @SerializedName("Logradouro")
        @Expose
        public String logradouro;
        @SerializedName("NrLogradouro")
        @Expose
        public String nrLogradouro;
        @SerializedName("Cidade")
        @Expose
        public String cidade;
        @SerializedName("CEP")
        @Expose
        public String cEP;
        @SerializedName("Telefone")
        @Expose
        public String telefone;
        @SerializedName("EMail")
        @Expose
        public String eMail;

    }
