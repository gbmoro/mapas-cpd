package cpd.ufrgs_maps.model;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteConstraintException;
import android.database.sqlite.SQLiteDatabase;
import android.support.annotation.NonNull;
import android.util.Log;

import java.util.ArrayList;
import java.util.List;

import cpd.ufrgs_maps.model.pojo.BuildingPOJO;

/**
 * Created by gabrieldoandroid on 31/05/17.
 */

public class BuildingDAO {

    public static final String TABLE_NAME = "Building";
    public static final String KEY_CODPREDIO = "codpredio";
    public static final String KEY_NOMEPREDIO =  "nomepredio";
    public static final String KEY_CODPREDIOUFRGS =  "codpredioufrgs";
    public static final String KEY_CAMPUS = "campus";
    public static final String KEY_DENOMINACAOCAMPUS = "denominacaocampus";
    public static final String KEY_LATITUDE = "latitude";
    public static final String KEY_LONGITUDE = "longitude";
    public static final String KEY_LOGRADOURO = "logradouro";
    public static final String KEY_NRLOGRADOURO = "nrlogradouro";
    public static final String KEY_CIDADE = "cidade";
    public static final String KEY_TELEFONE = "telefone";
    public static final String KEY_CEP = "cep";
    Context context;

    public BuildingDAO(Context context) {
        this.context = context;
    }

    public void insert(BuildingPOJO object) {
        long valueReturned = 0;

        SQLiteDatabase db = SQLiteConnection.getMyInstance(context).getWritableDatabase();
        ContentValues dataContent = getContentValueFromObject(object);

        if (object != null) {
                try {
                    if (dataContent.size() > 0) {
                        valueReturned = db.insert(TABLE_NAME, null, dataContent);
                        //Log.i(getClass().getName(), "The building was inserted -> " + valueReturned);
                    }
                } catch (SQLiteConstraintException | NullPointerException e) {
                    Log.e("Exception on insert - " + getClass().getName(), e.getMessage());
                }
        }
            db.close();

    }

    public List<BuildingPOJO> select() {

        List<BuildingPOJO> buildingList = new ArrayList<BuildingPOJO>();

        String query = "SELECT * FROM " + TABLE_NAME + ";";

        SQLiteDatabase db = SQLiteConnection.getMyInstance(context).getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        BuildingPOJO buildingTmp = null;

        while (c.moveToNext()) {
            buildingTmp = getDataFromCursor(c);
            if(buildingTmp != null) {
                buildingList.add(buildingTmp);
            }
        }

        c.close();
        db.close();

        //Log.i(getClass().getName(), "The size of buildings list is "+ buildingList.size());

        return buildingList;
    }

    public List<BuildingPOJO> select(double latitude, double longitude) {
        List<BuildingPOJO> buildingList = new ArrayList<BuildingPOJO>();

        String query = "SELECT * FROM " + TABLE_NAME + " WHERE "
                + BuildingDAO.KEY_LATITUDE + "= " + latitude + " and "
                + BuildingDAO.KEY_LONGITUDE + "= " + longitude + ";";

        SQLiteDatabase db = SQLiteConnection.getMyInstance(context).getReadableDatabase();
        Cursor c = db.rawQuery(query, null);

        BuildingPOJO buildingTmp = null;

        try {
            while (c.moveToNext()) {
                buildingTmp = getDataFromCursor(c);
                if (buildingTmp == null || buildingTmp.latitude == 0 ||
                        buildingTmp.longitude == 0) {
                    continue;
                } else {
                        buildingList.add(buildingTmp);
                    }
            }
        } catch (NullPointerException error1) {
            Log.e(getClass().getName(), "Nullpointer: " + error1.getMessage());
        }
        c.close();
        db.close();

        //Log.i(getClass().getName(), "The size of buildings list is "+ buildingList.size());

        return buildingList;
    }

    private BuildingPOJO getDataFromCursor(Cursor c){
        BuildingPOJO buildingPOJOTarget = new BuildingPOJO();
        try {
            if(c.getString(c.getColumnIndex(KEY_CODPREDIO))!=null) {
                buildingPOJOTarget.codPredio = c.getString(c.getColumnIndex(KEY_CODPREDIO));
            }
            buildingPOJOTarget.nomePredio = c.getString(c.getColumnIndex(KEY_NOMEPREDIO));
            buildingPOJOTarget.codPredioUFRGS = c.getString(c.getColumnIndex(KEY_CODPREDIOUFRGS));
            buildingPOJOTarget.campus = c.getInt(c.getColumnIndex(KEY_CAMPUS));
            buildingPOJOTarget.denominacaoCampus = c.getString(c.getColumnIndex(KEY_DENOMINACAOCAMPUS));
            buildingPOJOTarget.latitude = c.getDouble(c.getColumnIndex(KEY_LATITUDE));
            buildingPOJOTarget.longitude = c.getDouble(c.getColumnIndex(KEY_LONGITUDE));
            buildingPOJOTarget.logradouro = c.getString(c.getColumnIndex(KEY_LOGRADOURO));
            buildingPOJOTarget.nrLogradouro = c.getString(c.getColumnIndex(KEY_NRLOGRADOURO));
            buildingPOJOTarget.cidade = c.getString(c.getColumnIndex(KEY_CIDADE));
            buildingPOJOTarget.cEP = c.getString(c.getColumnIndex(KEY_CEP));
            buildingPOJOTarget.telefone = c.getString(c.getColumnIndex(KEY_TELEFONE));
        }catch(NullPointerException error){
            Log.e("BuildingDAO on select", error.getMessage());
            return null;
        }
        return buildingPOJOTarget;
    }

    private ContentValues getContentValueFromObject(@NonNull BuildingPOJO targetObj) {

        ContentValues data = new ContentValues();
        if(targetObj!=null){
                data.put(KEY_CODPREDIO, targetObj.codPredio);
                data.put(KEY_NOMEPREDIO, targetObj.nomePredio);
                data.put(KEY_CODPREDIOUFRGS, targetObj.codPredioUFRGS);
                data.put(KEY_CAMPUS, targetObj.campus);
                data.put(KEY_DENOMINACAOCAMPUS, targetObj.denominacaoCampus);
                data.put(KEY_LATITUDE, targetObj.latitude);
                data.put(KEY_LONGITUDE, targetObj.longitude);
                data.put(KEY_LOGRADOURO, targetObj.logradouro);
                data.put(KEY_NRLOGRADOURO, targetObj.nrLogradouro);
                data.put(KEY_CIDADE, targetObj.cidade);
                data.put(KEY_CEP, targetObj.cEP);
                data.put(KEY_TELEFONE, targetObj.telefone);
            }
        return data;
    }

}
