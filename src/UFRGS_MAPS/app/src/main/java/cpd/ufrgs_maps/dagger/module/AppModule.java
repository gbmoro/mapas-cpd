package cpd.ufrgs_maps.dagger.module;

import android.app.Application;

import javax.inject.Singleton;

import dagger.Module;
import dagger.Provides;

/**
 * This class is the better way to create a App Module, that will be used on NetModule.
 * Created by
 * https://adityaladwa.wordpress.com/2016/05/09/dagger-2-with-retrofit-and-okhttp-and-gson/
 */
@Module
public class AppModule {
    Application mApplication;

    /**
     * The constructor method
     * @param mApplication
     */
    public AppModule(Application mApplication) {
        this.mApplication = mApplication;
    }

    /**
     * Provides Application to Dagger Scope
     * @return
     */
    @Provides
    @Singleton
    Application provideApplication() {
        return mApplication;
    }
}