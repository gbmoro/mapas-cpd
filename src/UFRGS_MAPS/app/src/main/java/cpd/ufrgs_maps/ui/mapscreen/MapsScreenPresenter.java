package cpd.ufrgs_maps.ui.mapscreen;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.SearchView;
import android.util.Log;
import android.view.View;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.Marker;

import cpd.ufrgs_maps.model.BuildingDAO;
import cpd.ufrgs_maps.ui.listbuildingsscreen.ListBuildingsActivity;

/**
 * Created by gabri on 13/06/2017.
 */

public class MapsScreenPresenter implements MapsScreenContract.Presenter {

    Context mContext;
    SearchView searchView;
    MapsActivity mapsActivity;

    public MapsScreenPresenter(MapsActivity mapsActivity, SearchView searchView) {
        this.mContext = mapsActivity.getApplicationContext();
        this.mapsActivity = mapsActivity;
        this.searchView = searchView;
    }

    @Override
    public void findBySearchView() {
        this.searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                Log.i("SEARCH-QUERY", query);
                mapsActivity.loadHistory(query);
                return true;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                mapsActivity.loadHistory(newText);
                return true;
            }
        });
    }

    @Override
    public void markerEvent(GoogleMap mMap) {
        mMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
            @Override
            public void onInfoWindowClick(Marker marker) {
                String address = marker.getPosition().latitude + ", " + marker.getPosition().longitude;
                Log.i("onMarker" , address);

                Bundle mBundle = new Bundle();
                mBundle.putDouble(BuildingDAO.KEY_LATITUDE, marker.getPosition().latitude);
                mBundle.putDouble(BuildingDAO.KEY_LONGITUDE, marker.getPosition().longitude);

                Intent mIntent = new Intent(mapsActivity, ListBuildingsActivity.class);
                mIntent.putExtras(mBundle);
                mapsActivity.startActivity(mIntent);
            }
        });
    }

}
