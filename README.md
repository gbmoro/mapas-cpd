# --> UFRGS Mapas <-- #


## Description: ##

-   This is a UFRGS's app used to localize buildings, and other places of the Federal University of Rio Grande do Sul.

## Where are the artifacts? ##

```sh
$ ls
```
> support/    src/    engineering/    README.md

- support: tutorials, and others books about android development
- src: java, and XML codes (graphical interface)
- engineering: models, wireframes, ...

## About the team: ##

* Developers
    - Alan Winck 
    - Theodoro Motta 
    - Lucas Flores
    - Gabriel Moro

* Project Managers
    - Bárbara Arena
    - Thiago Motta

> Software Solutions Department - CPD (UFRGS)

